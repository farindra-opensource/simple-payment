# Farindra Payment

Ini adalah contoh aplikasi web list payment sederhana dengan menggunakan Framework Laravel 8, dimana didalamnya telah menggunakan fitur Event, Jobs dan Queue.

![farindra payment](https://farindra.com/images/portfolio/kledopayment.png "Philadelphia's Magic Gardens")

### [DEMO APLIKASI](https://simplepayment.farindra.xyz/)

## Installasi

Pastikan sistem operasi / server anda telah memenuhi kebutuhan minimum aplikasi sesuai dengan [Dokumentasi Framework Laravel 8](https://laravel.com/docs/8.x/deployment#server-requirements).

Pada root folder aplikasi,

install aplikasi via [Composer](https://getcomposer.org) pada terminal dengan menjalankan perintah
```
$ composer install
```

## Konfigurasi

Berikut beberapa konfigurasi agar aplikasi dapat berjalan dengan normal.

### Environment
edit file `.env` ( jika belum ada silahkan copy dari file `.env.example`) sesuaikan parameter di bawah ini dengan kebutuhan Anda.

```
...

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=databaseAnda
DB_USERNAME=usernameAnda
DB_PASSWORD=passwordAnda

BROADCAST_DRIVER=pusher
CACHE_DRIVER=file
QUEUE_CONNECTION=database
SESSION_DRIVER=file
SESSION_LIFETIME=120

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

...

```

kita menggunakan [Pusher](https://pusher.com) untuk realtime notification, silahkan sesuaikan settingan PUSHER_... `.env` di atas sesuai dengan credential Anda.

untuk membuat Unique Key aplikasi, jalankan perintah
```
$ php artisan key:generate
```

##### (opsional)

build asset via [npm](https://www.npmjs.com) untuk keperluan frontend dengan menjalankan perintah
```
$ npm install

// development mode
$ npm run dev

// production mode
$ npm run prod
```

---

### Migrasi Database

Jalankan perintah berikut untuk melakukan migrasi awal Database
```
$ php artisan migrate --seed
```

---

### Queue Worker
Sesuai dengan settingan `.env` di atas kita menggunakan **Database** untuk menampung Queue.

Jika diperlukan buka terminal baru. Jalankan perintah berikut untuk menjalankan Queue Worker pada channel `payment-delete`

```
$ php artisan queue:work --queue=payment-delete
```
*untuk menghentikan worker pada terminal bisa dengan shortcut* `Ctrl + c`  

---

### Serve / Running server
Jalankan aplikasi dengan perintah 
```
$ php artisan serve
```

## UNIT TEST
Sebelum menjalankan aplikasi disarankan agar menjalankan Unit Test untuk memastikan aplikasi telah melewati tahapan test setiap fitur dengan lancar tanpa kendala.

```
$ phpunit
```

## CHANGE LOG
- ver. 1.0.0 
  - Payment Feature List
  - Payment Create
  - Payment Delete

---

### Author

**Farindra E. P.**

* [gitlab/farindra](https://gitlab.com/farindra)
* [twitter/farindra](https://twitter.com/farindra)

### License

Copyright © 2021, [Farindra Project](https://farindra.com).
Released under the [MIT License](LICENSE).

***




