<?php

namespace Tests\Feature\Payment;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PaymentEndpointTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed();
    }

    /**
     * home endpoint test.
     *
     * @return void
     */
    public function test_home_redirect_to_payment()
    {
        $response = $this->get('/');

        $response->assertRedirect( route('payment.index') );
    }
    
    /**
     * test Index route
     *
     * @return void
     */
    public function test_payment_page()
    {
        $response = $this->get( route('payment.index') );

        $response->assertOk();
    }
    
    /**
     * test Render Payment Table
     *
     * @return void
     */
    public function test_render_table_should_return_view()
    {
        $response = $this->get( route('payment.renderTable') );

        $response->assertStatus(200)
                 ->assertSeeText('Payment Name', true);
    }
    
    /**
     * test Render Create Form
     *
     * @return void
     */
    public function test_render_create_form_should_return_view()
    {
        $response = $this->get( route('payment.renderCreate') );

        $response->assertStatus(200)
                 ->assertSeeText('Nama', true);
    }
    
    /**
     * testC reate Payement Success Case
     *
     * @return void
     */
    public function test_create_payment_should_success()
    {
        $response = $this->post( route('payment.store') , [
            'payment_name' => 'normal',
        ]);
        
        $response->assertStatus(200)
                 ->assertJson([
                    'success' => true,
                ]);
    }
    
    /**
     * test Delete Payemen tError Case
     *
     * @return void
     */
    public function test_delete_payment_should_error()
    {
        $response = $this->delete( route('payment.delete') , [
            'ids' => [0],
        ]);
        
        $response->assertStatus(400)
                 ->assertJson([
                    'error' => true,
                ]);
    }
    
}
