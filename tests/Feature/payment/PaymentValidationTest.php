<?php

namespace Tests\Feature\Payment;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Libraries\Kledo;

class PaymentValidationTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * test Name minimum character < 3
     *
     * @return void
     */
    public function test_validate_name_minimum_character()
    {
        $response = $this->post( route('payment.store') , [
            'payment_name' => 'no',
        ]);
        
        $response->assertStatus(400)
                 ->assertJson([
                    'error' => true,
                ]);
    }

    /**
     * test Name maximum character = 50
     *
     * @return void
     */
    public function test_validate_name_maximum_character()
    {
        $kledo = new Kledo;

        $response = $this->post( route('payment.store') , [
            'payment_name' => $kledo->generateRandomString(51),
        ]);
        
        $response->assertStatus(400)
                 ->assertJson([
                    'error' => true,
                ]);
    }
    
}
