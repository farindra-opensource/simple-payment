<?php

namespace Tests\Feature\Payment;

use App\Events\paymentEvent;
use App\Jobs\DeletePayments;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class PaymentQueueTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp(): void
    {
        parent::setUp();

        $this->seed();
    }

    /**
     * test delete payments jobs
     *
     * @return void
     */
    public function test_delete_payments_job_send_to_queue()
    {

        Queue::fake();
        
        $this->delete(route('payment.delete'),[
            'ids' => [2],
        ]);

        Queue::assertPushed(DeletePayments::class);

    }

    /**
     * test Fire Payement Event
     *
     * @return void
     */
    public function test_delete_payments_job_executed()
    {

        Bus::fake();

        $this->delete(route('payment.delete'),[
            'ids' => [1]
        ]);
        
        Bus::assertDispatched(DeletePayments::class);

    }
    
}
