<?php 

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;

/* payment routes */
Route::prefix('payment')->group(function () {

    Route::name('payment.')->group(function () {

        Route::get('/', [PaymentController::class, 'index'])->name('index');

        Route::post('/create', [PaymentController::class, 'store'])->name('store');

        Route::delete('/delete', [PaymentController::class, 'delete'])->name('delete');

        Route::get('/render-table', [PaymentController::class, 'renderTable'])->name('renderTable');

        Route::get('/render-create', [PaymentController::class, 'renderCreate'])->name('renderCreate');

        Route::get('/fire-payment-event', [PaymentController::class, 'firePaymentEvent'])->name('firePayementEvent');

    });

});

