<?php
return [

    'common' => [
        
    ],

    'success' => [
        'create' => 'Payment berhasil ditambahkan.',
        'delete' => 'Job Delete Payment berhasil dibuat.',
    ],

    'error' => [
        'create' => 'Payment gagal disimpan !',
        'delete' => 'Payment gagal dihapus !',
    ],

];