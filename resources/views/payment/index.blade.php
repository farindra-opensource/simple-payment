@extends('layouts.main')

@section('content')


<div class="uk-margin uk-width-xlarge uk-margin-auto uk-card uk-card-default uk-card-body uk-box-shadow-large">
    <div class="uk-card-header">
        <div class="uk-grid-small uk-flex-middle" uk-grid>
            <div class="uk-width-expand">
                <h1 class="uk-card-title uk-margin-remove-bottom kledo-title">{{ config('app.name')}}</h1>
                <span class="uk-text-meta uk-margin-remove-top kledo-subtitle">List</span>
                <button class="uk-align-right uk-button uk-button-primary btn-create" onclick="payment.renderCreate(this)">Tambah</button>
            </div>
        </div>
    </div>
    <div class="uk-card-body uk-height-large kledo-section">
        <h5>Loading ...</h5>
    </div>
    
    <div class="uk-card-footer">
        <span class="uk-text-green">INFO : &nbsp;<small class="kledo-info">-</small></span>
    </div>
</div>


@endsection

@push('scripts')
<script>

    /* define loading options */
    const loading = {
        image: "",
        text: "Loading...",
        textResizeFactor: 0.2,
        textColor: "#505050"
    };

    /* define related element */
    const sectionElement = $('.kledo-section');
    const infoElement = $('.kledo-info');
    const subtitleElement = $('.kledo-subtitle');

    

    /* define payment object methods */
    const payment = {

        renderTable: async (element) => {
           
            console.log('Rendering Table...');

            /* normalize element */
            sectionElement.html('');
            infoElement.html('-');
            $('.btn-create').prop('disabled', false);

            sectionElement.LoadingOverlay("show", loading);

            /* fetch render table view to section */
            await axios.get("{{ route('payment.renderTable') }}")
                
                .then(response => {

                    /* render section if fetch success */
                    if ( response.data) {

                        sectionElement.html(response.data);

                        subtitleElement.html('List');
                    }

                })
                .catch( (error) => {

                    infoElement.html(error);
                    
                    infoElement.html( renderAlert('danger',  error ) );

            });


            sectionElement.LoadingOverlay("hide");
            
            console.log('Rendering done.');

        },

        renderCreate: async (element) => {

            console.log('Rendering Create...');

            sectionElement.LoadingOverlay("show", loading);

            $(element).prop('disabled', true);

            /* fetch render form create view to section */
            await axios.get("{{ route('payment.renderCreate') }}")
                
                .then(response => {

                    /* render section if fetch success */
                    if ( response.data) {

                        sectionElement.html(response.data);

                        subtitleElement.html('Tambah Payment');
                    }

                })
                .catch( (error) => {

                    infoElement.html(error);

                    infoElement.html( renderAlert('danger',  error ) );

            });
            
            sectionElement.LoadingOverlay("hide");
            
            console.log('Rendering done.');

        },

        create: async (element) => {

            console.log('Saving...');

            $(element).prop('disabled', true);

            /* send request create payment  */
            await axios.post("{{ route('payment.store') }}", $("form").serialize()  )
                    
                .then(response => {

                    infoElement.html( renderAlert('success',  response.data.success ?? 'Success' ) );

                    $("#payment-create").trigger('reset');

                })
                .catch( (error) => {

                    console.log(error.response);

                    infoElement.html( renderAlert('danger',  error.response.data.error ?? error ) );

            });

            $(element).prop('disabled', false);

            console.log('Saving done.');
        },

        pick: async () => {

            /* Reference the Table. */
            const grid = document.getElementById("table-payment");
            
            /* Reference the CheckBoxes in Table. */
            const checkBoxes = grid.getElementsByTagName("INPUT");
            
            let data = [];

            /* Loop through the CheckBoxes and store the id */
            for (var i = 0; i < checkBoxes.length; i++) {

                if (checkBoxes[i].checked) {

                    data.push( checkBoxes[i].dataset.id );
                }

            }

            $('.button-delete').css('visibility', data.length > 0 ? 'visible' : 'hidden');

            infoElement.html( renderAlert('info', data.length + ' data selected' ) );

            console.log(data, data.length);

            return data;

        },

        delete: async (element) => {

            console.log('Deleting...');

            sectionElement.LoadingOverlay("show", loading);

            const data = { ids : await payment.pick() };

            /* send request delete payments  */
            await axios.delete("{{ route('payment.delete') }}", { data } )
                    
                .then(response => {

                    console.log(response);

                })
                .catch( (error) => {

                    try {

                        console.log(error);

                        infoElement.html( renderAlert('danger',  error.response.data.error ?? error ) );

                        sectionElement.LoadingOverlay("hide");

                    } catch (error) {

                        infoElement.html( renderAlert('danger', error ) );

                    }
                    
            });
            
            console.log('Deleting OK.');

        }

    };

    /* render alert element */
    function renderAlert(type, message) {

        type = type ?? 'primary';
        message = message ?? '-';

        if (message == '-') {

            return message;
        }

        return `<div class="uk-alert-`+ type +`" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p>`+ message  +`</p>
                </div>`;
    }

    $(document).ready( function () {

        /* subscribe payment deleted channel */
         Echo.channel('channel-payment-deleted')
            .listen('paymentEvent', async (data) => {

                console.log('Payment deleted channel triggered !', data);
                
                let infoData = data.data

                infoElement.html( renderAlert('info', 'Menghapus Data ...') );

                sectionElement.LoadingOverlay("text", 'Telah berhasil menghapus ' + (infoData.count ?? '-')  + ' Data');

                $('.kledo-table-row-' + infoData.id).remove();

                $('.button-delete').css('visibility',  'hidden');


                if (infoData.count == infoData.total) {

                    setTimeout(() => {

                        infoElement.html( renderAlert('success','Hapus Data Selesai') );

                        sectionElement.LoadingOverlay("hide");

                    }, 1500);

                }
                
        });

        /* init table */
        payment.renderTable();

    });

</script>
@endpush