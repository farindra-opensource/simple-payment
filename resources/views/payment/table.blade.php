<div class="uk-overflow-auto uk-height-small uk-height-medium">
    <table id="table-payment" class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
        <thead>
            <tr>
                <th class="uk-table-shrink">ID</th>
                <th class="uk-table-expand">Payment Name</th>
                <th class="uk-table-shrink">Delete</th>
            </tr>
        </thead>
        <tbody>

            @foreach ( $payments as $payment )
                <tr class="kledo-table-row-{{ $payment->id }}">
                    <td class="uk-text-nowrap" >{{ $payment->id }}</td>
                    <td class="uk-text-nowrap">{{ $payment->payment_name }}</td>
                    <td><input class="uk-checkbox" type="checkbox" data-id="{{ $payment->id }}"  onclick="payment.pick(this)"></td>
                </tr>
            @endforeach
            
        </tbody>
    </table>

</div>
<div class="uk-margin">
    <button class="uk-align-right uk-button uk-button-danger button-delete" onclick="payment.delete(this)" style="visibility:hidden;">Delete</button>
</div>