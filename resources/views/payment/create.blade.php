<div class="uk-height-medium">
    <form id="payment-create" class="uk-form-stacked">
        <div class="uk-margin">
            <label class="uk-form-label" for="form-stacked-text">Nama</label>
            <div class="uk-form-controls">
                <input class="uk-input" name="payment_name" type="text" placeholder="Nama Payment">
            </div>
        </div>
    
        <div class="uk-margin">
            <div class="uk-form-controls">
                <button onclick="payment.create(this)" class="uk-button uk-button-default button-save">Simpan</button>
                <a onclick="payment.renderTable(this)" class="uk-button uk-button-secondary">Batal</a>
            </div>
        </div>
    </form>
</div>
