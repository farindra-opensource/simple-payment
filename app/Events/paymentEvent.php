<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class paymentEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    
    /**
     * action for payment
     *
     * @var string info|deleted
     */
    public $action;
    
    /**
     * data
     *
     * @var array
     */
    public $data;

    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    public $queue = 'payment-delete';

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($action = 'info', $data = [] )
    {
        $this->action = $action;
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        switch ($this->action) {

            case 'deleted':
                $channel = 'channel-payment-deleted';
                break;
            default:
                $channel = 'channel-payment-info';

        }

        return new Channel($channel);
    }
}
