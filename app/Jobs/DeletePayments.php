<?php

namespace App\Jobs;

use App\Events\paymentEvent;
use App\Models\Payment;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeletePayments implements ShouldQueue
{
    use  Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 30;

    public $id;
    public $count;
    public $total;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $count, $total)
    {
        $this->id = $id;
        $this->count = $count;
        $this->total = $total;

        $this->onQueue('payment-delete');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // if ($this->batch()->cancelled()) {
        //     // Determine if the batch has been cancelled...

        //     return;
        // }
    
        if (Payment::find($this->id)->delete()) {

            paymentEvent::dispatch('deleted', [
                'id' => $this->id,
                'count' => $this->count,
                'total' => $this->total,
            ]);

        }

    }
}
