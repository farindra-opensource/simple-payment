<?php

namespace App\Http\Controllers;

use App\Libraries\Kledo;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    /**
     * our application library
     */
    public $kledo ;

    public function __construct(Request $request){
        
        /** define Kledo as global Library */
        $this->kledo = new Kledo();

    }
}
