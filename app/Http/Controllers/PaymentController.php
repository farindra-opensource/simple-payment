<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeletePaymentRequest;
use App\Http\Requests\StorePaymentRequest;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Events\paymentEvent;
use App\Jobs\DeletePayments;
use Illuminate\Bus\Batch;
use Illuminate\Support\Facades\Bus;
use Throwable;

class PaymentController extends BaseController
{

    /**
     * base blade viewq page
     */
    private $base_page = 'payment';
        
    /**
     * index of payment
     *
     * @return view
     */
    public function index() {

        $data = [
            'action' => 'list',
        ];

        return view('payment.index', $data);
    }
    
    /**
     * Store payment
     *
     * @param  Request $request
     * @return array   
     */
    public function store(StorePaymentRequest $request) {

        /* validate handling first error */
        if (isset($request->validator) && $request->validator->fails()) {

            return $this->kledo->setResponse('error', $request->validator->messages()->first(), NULL, false , 400  );
        }

        /* create payment */
        $payment = Payment::create([
            'payment_name' => $request->payment_name,
        ]);

        return $this->kledo->setResponse('success', __('payment.success.create'), $payment );
    }
    
    /**
     * Delete payment
     *
     * @param  mixed $request
     * @return array
     */
    public function delete(DeletePaymentRequest $request) {

        /* validate handling first error */
        if (isset($request->validator) && $request->validator->fails()) {

            return $this->kledo->setResponse('error', $request->validator->messages()->first(), NULL, false , 400  );
        }


        if ( !Payment::whereIn('id', $request->ids )->exists() ) {
    
            return $this->kledo->setResponse('error', __('payment.error.delete'), NULL, FALSE, 400);
        }

        $count = 1;
        $jobs = [];
        $start = now();

        foreach ($request->ids as $id) {
            
            DeletePayments::dispatch($id, $count, sizeof($request->ids))->delay($start->addSecond(1.5 * $count));

            $count++;
        }

        // foreach ($request->ids as $id) {
            
        //     $jobs[] = new DeletePayments($id, $count, sizeof($request->ids));

        //     $count++;
        // }

        // $batch = Bus::batch($jobs)->then(function (Batch $batch) {
            return $this->kledo->setResponse('success', __('payment.success.delete'), $batch ?? '');
        //     // All jobs completed successfully...
        // })->catch(function (Batch $batch, Throwable $e) {
        //     return $this->kledo->setResponse('error', __('payment.error.delete'), $e->getTraceAsString(), FALSE, 500);
        //     // First batch job failure detected...
        // })->finally(function (Batch $batch) {

        //     // The batch has finished executing...
        // })->name('Delete Payments')->dispatch();

        // while (($batch = $batch->fresh()) && !$batch->finished()) {
        //     paymentEvent::dispatch('deleted', [
        //         'id' => $batch->processedJobs(),
        //         'count' => $batch->processedJobs(),
        //         'total' => $batch->totalJobs,
        //     ]);
        //     sleep(1);
        // }
        
    }
    
    /**
     * render payment Table view
     *
     * @return view
     */
    public function renderTable() {

       $data = [
            'payments' => Payment::all(),
       ];

        return view("{$this->base_page}.table", $data)->render();
    }
    
    /**
     * render form Create view
     *
     * @return view
     */
    public function renderCreate() {

        return view("{$this->base_page}.create")->render();
    }

    public function firePaymentEvent(Request $request) {

        paymentEvent::dispatch($request->action, $request->data) ;
    }

}
